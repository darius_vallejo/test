//
//  ListModel.m
//  CogiMobile
//
//  Created by Developer on 7/04/15.
//  Copyright (c) 2015 Dario. All rights reserved.
//

#import "ListModel.h"

NSString * const kCopyright = @"copyright";
NSString * const kGeneralDescription = @"description";
NSString * const kItem = @"item";
NSString * const kDescription = @"description";
NSString * const kGuid = @"guid";
NSString * const kLink = @"link";
NSString * const kPubDate = @"pubDate";
NSString * const kTitle = @"title";



///////////////////////////////////////////////////////////
#pragma mark -ListModel

@interface ListModel()
@property (strong,nonatomic) NSDictionary *dictionary;
@property (strong,nonatomic) NSMutableArray *arrayItems;
@end

@implementation ListModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        self.dictionary = dictionary;
        [self setItems];
    }
    return self;
}

-(void)setItems{
    self.arrayItems = [[NSMutableArray alloc]init];
    for (NSDictionary *obj in self.dictionary[kItem]) {
        ItemsModel *arrayModel = [[ItemsModel alloc]initWithDictionary:obj];
        //            NSLog(@"array %@",self.arrayWords);
        [self.arrayItems addObject:arrayModel];
    }
}

-(NSArray *)getItems{
    return self.arrayItems;
}

-(NSString*)copyright{
    return [self dictionary][kCopyright];
}

-(NSString*)generalDescription{
    return [self dictionary][kGeneralDescription];
}


@end

///////////////////////////////////////////////////////////
#pragma mark -ItemsModel

@interface ItemsModel()
@property (strong,nonatomic) NSDictionary *dictionary;
@end

@implementation ItemsModel
-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    if (self = [super init]) {
        self.dictionary = dictionary;
    }
    return self;
}

-(NSString *)description{
    return [self dictionary][kDescription];
}

-(NSString*)guid{
    return [self dictionary][kGuid];
}

-(NSString*)link{
    return [self dictionary][kLink];
}

-(NSString*)pubDate{
    return [self dictionary][kPubDate];
}

-(NSString*)title{
    return [self dictionary][kTitle];
}

@end

