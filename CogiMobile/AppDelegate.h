//
//  AppDelegate.h
//  CogiMobile
//
//  Created by Developer on 7/04/15.
//  Copyright (c) 2015 Dario. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

