//
//  WebsiteViewController.m
//  CogiMobile
//
//  Created by Developer on 7/04/15.
//  Copyright (c) 2015 Dario. All rights reserved.
//

#import "WebsiteViewController.h"

@interface WebsiteViewController ()
@property (nonatomic,strong) UIWebView *web;
@end

@implementation WebsiteViewController{
    UIActivityIndicatorView * activity;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activity.frame = CGRectMake(0, 0, 30, 30);
    activity.center = CGPointMake(self.view.frame.size.width * 0.5f, self.view.frame.size.height * 0.4f);
    activity.hidesWhenStopped = YES;
    [activity startAnimating];
    self.navigationItem.title = self.stUtitle;

    self.web = [[UIWebView alloc]initWithFrame:self.view.frame];
    [[self view] addSubview:self.web];
    [[self view] addSubview:activity];
    self.web.delegate = self;
    [self.web loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.stUrl]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [activity stopAnimating];
}

@end
