//
//  WebsiteViewController.h
//  CogiMobile
//
//  Created by Developer on 7/04/15.
//  Copyright (c) 2015 Dario. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebsiteViewController : UIViewController<UIWebViewDelegate>

@property NSString * stUrl;
@property NSString * stUtitle;
@end
