//
//  ListModel.h
//  CogiMobile
//
//  Created by Developer on 7/04/15.
//  Copyright (c) 2015 Dario. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListModel : NSObject

@property (nonatomic,readonly) NSString* copyright;
@property (nonatomic,readonly) NSString* generalDescription;
@property (nonatomic,readonly) NSArray* item;
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
-(NSArray*)getItems;
@end

@interface ItemsModel : NSObject
@property (nonatomic,readonly) NSString* description;
@property (nonatomic,readonly) NSString* guid;
@property (nonatomic,readonly) NSString* link;
@property (nonatomic,readonly) NSString* pubDate;
@property (nonatomic,readonly) NSString* title;
-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end