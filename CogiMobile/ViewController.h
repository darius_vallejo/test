//
//  ViewController.h
//  CogiMobile
//
//  Created by Developer on 7/04/15.
//  Copyright (c) 2015 Dario. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPRequestOperationManager.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *table;

@end

