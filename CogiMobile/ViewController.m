//
//  ViewController.m
//  CogiMobile
//
//  Created by Developer on 7/04/15.
//  Copyright (c) 2015 Dario. All rights reserved.
//

#import "ViewController.h"
#import "XMLDictionary.h"
#import "ListModel.h"
#import "ItemsTableViewCell.h"
#import "WebsiteViewController.h"

@interface ViewController ()

@end

@implementation ViewController{
    ListModel *model;
    ItemsModel *items;
    UIActivityIndicatorView *activity;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activity.frame = CGRectMake(0, 0, 30, 30);
    activity.center = CGPointMake(self.view.frame.size.width * 0.5f, self.view.frame.size.height * 0.4f);
    activity.hidesWhenStopped = YES;
    [activity startAnimating];
    [[UINavigationBar appearance] setBackgroundColor:[UIColor colorWithRed:84.0f/255.0f green:118.0f/255.0f blue:158.0f/255.0f alpha:1]];
    [self.view addSubview:activity];
    [self nowLoadClient];
}


///////////////////////////////////////////////////////////////////////
-(void)nowLoadClient{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPRequestSerializer * requestSerializer = [AFHTTPRequestSerializer serializer];
    AFHTTPResponseSerializer * responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSString *ua = @"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25";
    [requestSerializer setValue:ua forHTTPHeaderField:@"User-Agent"];
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/rss+xml", nil];
    manager.responseSerializer = responseSerializer;
    manager.requestSerializer = requestSerializer;
    [manager GET:@"http://webassets.scea.com/pscomauth/groups/public/documents/webasset/rss/playstation/Games_PS3.rss" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"rest %@",responseObject);
        NSData * data = (NSData *)responseObject;
        NSString* xmlData = [NSString stringWithCString:[data bytes] encoding:NSISOLatin1StringEncoding];
        NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:xmlData];
        [self loadAllContent:xmlDoc[@"channel"]];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to connect to server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        NSLog(@"rest %@",error);
    }];
}

-(void)loadAllContent:(NSDictionary*)responseObject{
    model = [[ListModel alloc] initWithDictionary:responseObject];
//    self.table.rowHeight = UITableViewAutomaticDimension;
//    self.table.estimatedRowHeight = 160.0f;
    [self.table reloadData];
    [activity stopAnimating];
}


#pragma mark -TableViewDelegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  model.getItems.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ItemsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ItemsCell"];
    items = (ItemsModel*)model.getItems[indexPath.item];
    cell.lblTitle.text = items.title;
    cell.lblDescription.text = [items.description componentsSeparatedByString:@"."][0];
    //[items.title stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    WebsiteViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WebsiteView"];
    ItemsModel *item = (ItemsModel*)model.getItems[indexPath.item];
    controller.stUrl = item.link;
    controller.stUtitle = item.title;
    [self.navigationController pushViewController:controller animated:YES];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    items = (ItemsModel*)model.getItems[indexPath.item];
    UILabel *comment = [[UILabel alloc]initWithFrame:CGRectMake(8, 0, self.view.frame.size.width - 20, 0)];
    comment.lineBreakMode = NSLineBreakByWordWrapping;
    comment.numberOfLines = 0;
    comment.font = [UIFont fontWithName:@"Lato-Regular" size:17];
    comment.text = [items.description componentsSeparatedByString:@"."][0];
    [comment sizeToFit];
    CGFloat hight = comment.frame.size.height;
    return 42 + hight;
}


//TODO: Animate Table
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    CATransform3D rotation;
    rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    rotation.m34 = 1.0/ -600;
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    cell.layer.transform = rotation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.5];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
    
}

@end
