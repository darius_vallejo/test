//
//  ItemsTableViewCell.h
//  CogiMobile
//
//  Created by Developer on 7/04/15.
//  Copyright (c) 2015 Dario. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@end
